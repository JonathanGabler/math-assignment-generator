<?php
// include Database connection file
include("../config/db_connection.php");
$assignmentID = $_GET['assignmentValue'];
// Design initial table header
$data = '<table class="table table-bordered table-striped">
						<tr>
							<th>Problem ID</th>
							<th>Problem</th>
						</tr>';

//GET ALL PROBLEMS IN CURRENT ASSIGNMENT
$query = "SELECT `content` , `pid`
    FROM `problem` 
    JOIN mathprobdb.assignment_problems ON mathprobdb.problem.pid = mathprobdb.assignment_problems.problem_id
    WHERE `mathprobdb`.assignment_problems.assignment_id = $assignmentID";
//Execute query 2
if (!$result = mysqli_query($con, $query)) {
    exit(mysqli_error($con));
}

// if query results contains rows then fetch those rows
if(mysqli_num_rows($result) > 0)
{
    $number = 1;
    while($row = mysqli_fetch_assoc($result))
    {
        $data .= '<tr>
				<td>'.$row['pid'].'</td>
				<td>'.$row['content'].'</td>
    		</tr>';
        $number++;
    }
}
else
{
    // records now found
    $data .= '<tr><td colspan="6">There is no problems in this assignment!</td></tr>';
}

$data .= '</table>';

echo $data;
?>