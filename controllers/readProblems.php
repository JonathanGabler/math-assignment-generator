<?php
// include Database connection file
include("../config/db_connection.php");

//get the assignment ID
$assignmentID = $_GET['assignmentValue'];

echo("<script>console.log('AssignmentID: ".$assignmentID."');</script>");

// Design initial table header
$data = '<table class="table table-bordered table-striped">
						<tr>
							<th>Problem ID</th>
							<th>Problem</th>
							<th>In Assignments</th>
							<th>Add/Remove to current Assignment </th>
						</tr>';
//GET ALL MATH PROBLEMS FROM DATABASE TABLE PROBLEMS
$query = "SELECT * FROM mathprobdb.problem";
//RUN THE QUERY
if (!$result = mysqli_query($con, $query)) {
    exit(mysqli_error($con));
}
//GET ALL PROBLEMS IN CURRENT ASSIGNMENT FROM TABLE ASSIGNMENT PROBLEMS
$query2 = "SELECT `pid` 
    FROM `problem` 
    JOIN mathprobdb.assignment_problems ON mathprobdb.problem.pid = mathprobdb.assignment_problems.problem_id
    WHERE mathprobdb.assignment_problems.assignment_id = $assignmentID";
//Execute query 2
if (!$result2 = mysqli_query($con, $query2)) {
    exit(mysqli_error($con));
}
//Check results of query2
if(mysqli_num_rows($result2) > 0)
{
    $number = 0;
    $problemsInAssigment = array();
    while($row = mysqli_fetch_assoc($result2))
    {
        $problemsInAssigment[$number]= $row['pid'];
        //echo $row['pid'];
        $number++;
    }
}
else
{
    $problemsInAssigment[]=0;
}

// RETURN PROBLEMS AND LIST OF ASSIGNMENTS PROBLEM BELONGS TO BACK TO VIEW
if(mysqli_num_rows($result) > 0)
{
    $number = 1;
    $onClickAction = 0;
    while($row = mysqli_fetch_assoc($result))
    {
        $problemID=$row['pid'];
        $getAssignmentsQuery = "SELECT `id` FROM `assignments` JOIN mathprobdb.assignment_problems ON mathprobdb.assignments.id = mathprobdb.assignment_problems.assignment_id WHERE mathprobdb.assignment_problems.problem_id = $problemID ";
        //Execute query to find assignments problem is in.
        if (!$result3 = mysqli_query($con, $getAssignmentsQuery)) {
            exit(mysqli_error($con));
        }
        //Check results of query3
        $problemInAssigments = array();
        if(mysqli_num_rows($result3) > 0)
        {
            //Add the assignments the problem is in into array
            $assignmentNum = 0;
            while($r = mysqli_fetch_assoc($result3))
            {
                $problemInAssigments[$assignmentNum]= $r['id'];
                $assignmentNum++;
            }
        }
        else
        {
            //The problem is in no assignments
            $problemsInAssigment[]=0;
        }
        if(in_array($row['pid'],$problemsInAssigment)){
            //Problem is in the current assignment
            $onClickAction = 1;
        }
        else{
            //Problem is NOT in current assignment
            $onClickAction = 2;
        }
        //Table column values
        $data .= '<tr>
				<td>'.$row['pid'].'</td>
				<td>'.$row['content'].'</td>
				<td>'.implode(",",$problemInAssigments).'</td>
				<td>
					<button onclick="isInAssignment('.$onClickAction.','.$row['pid'].','.$assignmentID.')" class="btn btn-sm btn-primary"><i class="fas fa-file-alt"></i></button>
				</td>
    		</tr>';
        $number++;
    }
}
else
{
    // There was no problems found in the database
    $data .= '<tr><td colspan="6">Records not found!</td></tr>';
}

$data .= '</table>';

echo $data;
?>