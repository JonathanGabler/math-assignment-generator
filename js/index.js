// READ ALL PROBLEMS AND PASS TO TABLE
function readProblems(assignmentVal) {
    $.get("../controllers/readProblems.php", {assignmentValue: assignmentVal}, function (data, status) {
        $(".problemRecords_content").html(data);
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
    });
}

// READ current assignment page
function getCurrentAssignment() {
    const currentAssignment = $("#assignment").val();
    console.log("getCurrentAssignment Called with ID:" + currentAssignment);
    $.get("../controllers/readCurrentAssignment.php", {
        assignmentValue: currentAssignment
    },
        function (data, status) {
        $(".assignmentProblems_content").html(data);
        $("#view_current_assignment_modal").modal("show");
        MathJax.Hub.Typeset();
    });
}

// CREATE A NEW PROBLEM
function addNewProblem() {
    // get values
    var content = $("#problemContent").val();

    // Add record
    $.post("../controllers/createProblem.php", {
        content: content,
    }, function (data, status) {
        // close the popup
        $("#add_new_record_modal").modal("hide");

        // read records again
        setAssignment();

        // clear fields from the popup
        $("#problemContent").val("");
    });
}

// CREATE NEW ASSIGNMENT
function addNewAssignment(titleName) {
    // get values
    var title = titleName;

    // Add record
    $.post("../controllers/createAssignment.php", {
        title: title
    }, function (data, status) {
        // close the popup
        $("#add_new_assignment_modal").modal("hide");
        // clear fields from the popup
        $("#assignmentTitle").val("");
        //Set the current assignment to the new assignment
        const newListItemID = data.valueOf();
        $("#hiddenCurrentAssignment").val(newListItemID);
        location.reload();

    });
}

// CHECK IF ASSIGNMENT EXISTS ALREADY
function checkAssignmentExists() {
    console.log("checkAssignmentExist Called");
    // get values
    var title = $("#assignmentTitle").val();

    // Add record
    $.post("../controllers/assignmentExists.php", {
        title: title
    }, function (data, status) {
        console.log(data);
        if(data === "Exists"){
            alert("Assignment already exists!");
            location.reload()
        }
        else{
            addNewAssignment(title);
        }
    });
}
// ADD PROBLEM TO ASSIGNMENT
function addProblemToHomework(problemID, assignmentID) {
    // log values
    console.log("Problem: " + problemID + " Added to assignment: " +assignmentID);

    // Add record
    $.post("../controllers/addProblemToAssignment.php", {
        problemID: problemID,
        assignmentID: assignmentID
    }, function (data, status) {
        //Update the view
        setAssignment();
    });
}

// REMOVE PROBLEM FROM ASSIGNMENT
function removeProblemToHomework(problemID, assignmentID) {
    // log values
    console.log("Problem: " + problemID + " Removed from assignment: " +assignmentID);

    // post record
    $.post("../controllers/removeHomeworkProblem.php", {
        problemID: problemID,
        assignmentID: assignmentID
    }, function (data, status) {
        //Update the view
        setAssignment();
    });
}

// GET THE VALUE OF CURRENT ASSIGNMENT
function setAssignment() {
    // get values
    console.log("SetAssignment was called");
    console.log("value of hidden html selected list: "+$("#hiddenCurrentAssignment").val());
    const currentAssignment = $("#hiddenCurrentAssignment").val();
    $("#assignment").val(currentAssignment);
    readProblems(currentAssignment);
}

// GET THE VALUE OF CURRENT ASSIGNMENT
function updateSetAssignment() {
    // get values
    console.log("updateSetAssignment was called");
    const currentAssignment = $("#assignment").val();
    $("#hiddenCurrentAssignment").val(currentAssignment);
    console.log("value of hidden html selected list: "+$("#hiddenCurrentAssignment").val());
    readProblems(currentAssignment);
}

// CHECK IF PROBLEM IS IN CURRENT ASSIGNMENT
function isInAssignment(inAssignment,problemID,assignmentID ){
    console.log("onClickValue = "+ inAssignment +" Problem: " + problemID + " Added to assignment: " +assignmentID);

    if(inAssignment === 2){
        addProblemToHomework(problemID,assignmentID);
    }
    else if(inAssignment === 1) {
        removeProblemToHomework(problemID, assignmentID);
    }
    else{
        alert("Something went horribly wrong");
    }
}

// ON PAGE LOAD
$(document).ready(function () {
    setAssignment();
});
