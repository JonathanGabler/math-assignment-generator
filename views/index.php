<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Project2-Index</title>

    <!-- Bootstrap CSS File  -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script type="text/javascript">
        window.MathJax = {
            tex2jax: {
                inlineMath: [["\\(", "\\)"]],
                processEscapes: true
            }
        };
    </script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
    </script>
</head>
<body>
<!-- Content Section -->
<div class="container">
    <!--Title Div-->
    <div class="row">
        <div class="col-md-12">
            <h2><b>Math Homework Generator</b></h2>
            <br>
        </div>
    </div>
    <!--ASSIGNMENT DROP DOWN-->
    <div class="row">
        <div class="col-md-12">
            <h4>Assignments</h4>
            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group mr-2" role="group" aria-label="Second group">
                    <?php
                    // include Database connection file
                    include("../config/db_connection.php");
                    $sql = "SELECT * FROM assignments ORDER BY `id` DESC ";
                    $result = mysqli_query($con, $sql);
                    $number=0;
                    ?>
                    <label for="assignment">Current Assignment: </label>
                    <select id="assignment" name="assignment" onchange="updateSetAssignment()">
                        <?php
                        while($row = mysqli_fetch_array($result)){
                            echo "<option value='" . $row['id'] . "' selected='selected'>" . $row['title'] . "</option>";
                            $number++;
                        }
                        echo "<input type='hidden', id='hiddenCurrentAssignment' value='$number'>"
                        ?>
                    </select>
                </div>
                <div class="btn-group mr-2" role="group" aria-label="Third group">
                    <button class="btn btn-sm btn-info" data-toggle="modal" onclick="getCurrentAssignment()">View current assignment</button>
                </div>
                <div class="btn-group mr-2" role="group" aria-label="First group">
                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#add_new_assignment_modal">Create new Assignment</button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!--MATH PROBLEMS TABLE-->
    <div class="row">
        <div class="col-md-12">
            <h4>Math Problems</h4>
            <div class="pull-right">
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#add_new_record_modal">Create new problem</button>
            </div>
        </div>
    </div>
    <br>
    <!--Records from database-->
    <div class="row">
        <div class="col-md-12">
            <div class="problemRecords_content"></div>
        </div>
    </div>
</div>
<!-- /Content Section -->
<!-- Bootstrap Modals -->
<!-- Modal - Add New Record/Problem -->
<div class="modal fade" id="add_new_record_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="problemContent">Problem</label>
                    <textarea type="text" id="problemContent" placeholder="Enter Problem Here" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-sm btn-success" onclick="addNewProblem()">Create Problem</button>
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->
<!-- Modal - View Current Assignment-->
<div class="modal fade" id="view_current_assignment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="assignmentProblems_content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->
<!-- Modal - Add New Record/Assignment -->
<div class="modal fade" id="add_new_assignment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="assignmentTitle">Title</label>
                    <input type="text" id="assignmentTitle" placeholder="Assignment Title" class="form-control"/input>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-sm btn-success" onclick="checkAssignmentExists()">Create Assignment</button>
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->
<!-- Jquery JS file -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Bootstrap JS file -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>

<!-- Custom JS file -->
<script type="text/javascript" src="../js/index.js"></script>
</body>
</html>